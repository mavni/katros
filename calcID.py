import sys, os, random, time

debug = False

if debug:

    Folder_name = '/home/katros/runs'
    ID = 1234567890

else:
    Folder_name = sys.argv[1]
    ID = sys.argv[2]

def calcID(Folder_name, ID):
    # Wait for up to 30 seconds to mimic real life calculation
    time.sleep(random.randint(1, 30))
    # Succeed in 80% of the calculations
    randomNumber = random.randint(1, 100)
    if debug:
        print (randomNumber)
    if randomNumber <= 80:
        if debug:
            print ("Calculation successfully completed")
            print ("Job ID is: ", ID)
            print ("Folder created: ", ID)
        # If folder already exists, do npt re-create it
        if os.path.exists(os.path.abspath(os.path.join(Folder_name, str(ID)))):
            if debug:
                print 'Folder exits'
        else:
            # Create a folder with the ID as name
            os.makedirs(os.path.abspath(os.path.join(Folder_name, str(ID))))

    else:
        if debug:
            print ("Calculation failed")
        # Exit with an Exit Code of '1'
        exit(1)


calcID(Folder_name, ID)