import subprocess, Queue, multiprocessing, sys
from multiprocessing import Pool

# For debug prints, set debug = True
debug = False

if debug:

    Folder_name = str('/home/katros/runs')
    Ids_file = str('IDs')
    num_of_processes = 8
else:

    # Destination for successful calculations as first variable of the command
    Folder_name = str(sys.argv[1])

    # Ids_file as second variable of the command
    Ids_file = str(sys.argv[2])

    # Number of instances as third variable of the command
    num_of_processes = int(sys.argv[3])

# Create queue
q = Queue.Queue()


def getJobs():
    # Read IDs from file
    with open(Ids_file, 'r') as f:
        for l in f:
            if l not in q.queue:
                q.put(l)
        return q


def removeID(qid):
    with open(Ids_file, 'r+') as f:
        # Read content from file
        content = f.readlines()
        # Remove successfully calculated ID
        content.remove(qid)
        # Go back to the beginning of the file
        f.seek(0)
        # Delete the content of the file
        f.truncate()
        # Write fixed content back to file
        for l in content:
            f.writelines(str(l))
        # Close the file
        f.close()


def calculate():
    # Grab an ID from the queue
    qid = q.get()

    sp = subprocess.Popen(['python', 'calcID.py', Folder_name, qid], stdout=subprocess.PIPE)
    data = sp.communicate()[0]

    # Variable to hold each calculations Exit Code
    ec = sp.returncode

    # Check whether the calculation was successfull or not
    if ec == 0:

        if debug:
            print data

        # remove ID from queue if successful
        q.task_done()

        # remove from Ids file
        removeID(qid)

        if debug:
            print q.unfinished_tasks, ' IDs left'

        return

    # Calculation ended with failure
    elif ec == 1:
        if debug:
            print data
        # Write ID back to file
        q.put(qid)

        return

    # If any other reason for exit has occured, display the Exit Code
    else:
        if debug:
            print 'Exit Code %s' % ec
        # Write ID back to file
        q.put(qid)

def doTheMagic():
    # Check if any tasks are available for calculation
    while not q.empty():
        # Create a pool of processes
        pool = multiprocessing.Pool(num_of_processes)
        # Create num_of_processes threads
        #for i in range(num_of_processes):
        results = []
        p = pool.apply_async(calculate(), range(num_of_processes), callback=results.append)

    # If running low on IDs in the queue, fetch new ones from the file
    if q.unfinished_tasks < 10:
        getJobs()
        doTheMagic()

if __name__ == '__main__':

    doTheMagic()